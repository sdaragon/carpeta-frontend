module.exports = {
  /* Use the Desy default config */
  presets: [require('desy-html/config/tailwind.config.js')],
  content: ['./node_modules/desy-html/src/**/*.html',
              './node_modules/desy-html/src/**/*.njk',
              './src/**/*.html',
              './src/**/*.njk',
              './docs/**/*.html',
              './docs/**/*.njk'
            ],
  safelist: [
            'dev', // used in body tag
            'has-offcanvas-open',
            'has-dialog',
            'dialog-backdrop',
            'dialog-backdrop.active',
            'focus',
            'headroom',
            'headroom--pinned',
            'headroom--unpinned',
            'headroom--top',
            'headroom--not-top',
            'headroom--bottom',
            'headroom--not-bottom',
            'headroom--frozen'
            ],
  theme: {
    extend:{
      screens: {
        // '2xl': '1536px',
        // '3xl': '1900px'
      },
      minHeight: {
       '48': '12rem',
       '96': '24rem',
      },
      zIndex: {
       '75': 75,
       '10000': 10000,
      }
    }
  }
}
